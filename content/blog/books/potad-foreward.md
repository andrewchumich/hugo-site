+++
title = "Prisoners of the American Dream: Foreward"
date = "2020-04-24"
categories = ["books"]
tags = ["Mike Davis", "Prisoners of the American Dream", "history"]
+++

Why this book? Why now? I've been itching to dive into Mike Davis' work for a while. *Late Victorian Holocosts* has sat dormant on my shelf and could never get around to it. His work spans nearly everything I'm interested in: ecology, land use, Southern California, Marxism, economic imperialism, and organized labor. What's not to like? His books were recently on sale and a ordered a few more. I'm going to start with *Prisoners of the American Dream* (*PotAD*). 

In the foreward to *PotAD*, a collection of essays, he lays out a few questions that he hopes to answer. Why is organized labor particularly weak in the United States? Labor has failed to advance its short and long term interests and continues to be clobbered by an increasingly organized right wing. It has failed to work within the political system and gain meaningful power within the Democratic party and it weilds next to no power outside of the political sphere.

The power of organized capital in the US could not exist without consent of large parts of the working class. Davis will argue in the first half of the book that:

> The ballast of capital's hegemony in American history has been the repeated, autonomous mobilizations of the mass middle strata in defense of petty accumulation and entrepreneurial opportunity. (xi)

The second half will be to place the crises of the 70s and the emergence of Reaganism into this narrative. I am particularly excited about that section. This is the mileu in which my parents' political consiousness was formed. Their understanding of what is both politically possible and necessary is perfectly encapsulated by Davis' description of a federal government whose primary responsibility is to "provide welfare to the well-to-do and preserve a dymnamic *frontier* [emphasis mine] of entrepreneurial, professional and rentier opportunity." (xi-xii) 

The future of working within a rightward-drifting Democratic party looks bleak. They have given up "any serious appeal to full employment politics or social equality." (xii) Davis believes that the Left must organize an independent "rainbow coalition" against American imperialism. From the persective of today, after the failure to stop the invasion of Iraq and the War on Terror at large, the left has failed absolutely.

Written in the mid 80s, Davis is inserting himself into the Left's strategic debates of the time. He saw a labor movement that was simultaneously aware of their position, but lacked confidence to act collectively. He wants to construct a material basis for the popularity of right-wing ideology.

This should be a good read. Davis' writing can be dense, but I think I'm ready for it. I would like to keep writing updates as I read. Giving thoughts and solidifying lessons through repetition. I want to read Davis' telling of American labor history so I can better differentiate the sections of the US working class.
