+++
title = "City of Quartz"
date = "2020-05-23"
lastmod = "2020-06-01"
categories = ["books"]
tags = ["Mike Davis", "history", "City of Quartz"]
+++


# Table of Contents

1.  [Introduction/Motivation](#org240f5b8)
2.  [Week 1 (<span class="timestamp-wrapper"><span class="timestamp">&lt;2020-06-02 Tue&gt;</span></span>)](#orge19311c)
    1.  [Notes](#orgdaa6e2f)
    2.  [Discussion agenda](#orgc107f80)
    3.  [Weekly Questions](#org298d9ba)
    4.  [Preface](#org133ab5b)
        1.  [Questions](#org74cf77d)
        2.  [Summary](#orgefd686a)
    5.  [Prologue](#orge1341e0)
        1.  [Questions](#org64d1c55)
        2.  [Summary](#org0972f8b)
3.  [Week 2 <span class="timestamp-wrapper"><span class="timestamp">&lt;2020-06-09 Tue&gt;</span></span>](#orge4eeffc)
    1.  [*Sunshine or Noir?*](#orgfd49e4e)
    2.  [Strong Towns: The More We Grow, the Poorer we Become](#org3ac71df)
        1.  [An Insolvency Crisis](#org6f8d000)
        2.  [A Lack of Wealth](#orgb615234)
        3.  [Simple Math](#orgd5182ce)
        4.  [Strong Towns Approach](#org6c8f3c9)
4.  [Supplemental](#org56e09c7)
    1.  [Books](#org0bb77eb)
    2.  [Movies](#orgf6ac37f)
5.  [Misc quotes](#org1a1e09e)
6.  [Links (Articles, videos, etc.)](#orgf76a1a3)


<a id="org240f5b8"></a>

# Introduction/Motivation

The Left is impotent in the Treasure Valley. Sure, our politics are becoming dominated by the Democratic party, but outside of sporatic semi-successful social justice campaigns and rallies, power is still wielded by the economic elite. We are beginning to see cracks in a community that often appears endlessly prosperous: an increasingly globalized real estate market leaving homeowners sour to growth, skyrocketing rents, and stagnant wages. What I think the Left in our region is missing is a detailed understanding of the economic and social forces that shape the Treasure Valley. What sort of people live here? What do their origins tell us about how they will react to economic stresses? Are there distinct periods of migrations we can identify? How can we expect long-time homeowners to react to rapid growth and urbanization? Can their anger be harnessed for good or are we doomed to repeat southern California's mistakes? Does there exist meaningful geographic stratifications in the region both now and historically? Where do/did the rich live? Where do/did the poor live? What are the economic foundations of the region?

Any attempt at creating a powerful working-class political movement will need good answers to these questions and I hope this book will help us get started.

-&#x2014;

It's common to hear people say that they moved from California and are afraid that Boise is going to make the same mistakes, but what do they mean? This is usually in reference to higher property taxes, increased density, and liberal social policies. What really happened in Southern California? My current understanding:

Post WW2, Los Angeles [abandoned the traditional development](https://www.strongtowns.org/curbside-chat-1/2015/12/14/americas-suburban-experiment) pattern and adopted suburban development on a large scale. Over time, this unproductive way of building required the region to continually increase taxes. 

This pattern of development was not only economically destructive, it was socially harmful as well. Suburban, auto-oriented, living led to a variety of anti-social behaviours. People had fewer interactions with neighbors and random encounters with strangers. Small, incremental changes to the built environment became rare. Development increasingly could only be accomplished by large and powerful corporations, leaving people alienated and powerless. Homeowners clung to their small, picket-fenced refuges, the only place they felt under their control. 

These people have very little experience democratically resolving problems. Their strategy is to move to the next suburb when things get too crowded and difficult. Where would they have learned anything different? Their environment encourages isolation and protectiveness. In combination with fear of an ever-expanding poor minority population, developers profited from wealthy white homeowners' willingness to move to the next paved-over desert neighborhood called "Sunrise Chateau".


<a id="orge19311c"></a>

# Week 1 (<span class="timestamp-wrapper"><span class="timestamp">&lt;2020-06-02 Tue&gt;</span></span>)

**Reading** Preface & Prologue


<a id="orgdaa6e2f"></a>

## Notes

Cam wants a leftist history and critique of suburbia

Cameron wants some tangible actions to take. Learning about formal socialist perspectives.

-   Who are the antagonists and protagonists. Tommy Ahlquist


<a id="orgc107f80"></a>

## Discussion agenda

Though I have some questions below, I want the first week's discussion to focus on what we hope to get out of this book. Some scattered thoughts and ideas:

-   From what we've read so far, what sort of connections could we make to Boise?
-   How familiar are we with LA and the surrounding region? We should do a quick google maps tour to get some bearings.
    -   Have we spent much time there?
    -   How do we sort out the real from the myth?
-   What parts of LA/California history are we particularly interested in?
    -   Ashley: California ballot initiatives?

> 
> 
> “The rabid polarization of the Southern California suburbs against the campuses and ghettoes, together with iron-heeled power of the corporate growers, created a ripe political context for new modes of right-wing mobilization. Indeed, one of the most important sequences of experiences through which the New Right came to recognize its power was the series of referenda that united a middle-class and white working-class backlash against integrated housing (1964–65), abolition of the death penalty (1965, 1976), the rights of farm labor (1972), school busing (1979) and property taxes (1978).” (*Prisoners of the American Dream* p.163)

Excerpt From: Mike Davis. “Prisoners of the American Dream.”

-   Cam S: Worker and Socialist movements? Riots and uprisings?
-   Cam C: Other social justice movments?

-   Chapter 3 seems to be the most important (at least for what I'm interested in). Maybe we should aim for getting through that chapter and reassessing if the rest of the book is worth our time or we should pivot to something else.
-   If we finish the book, I would like to write something and submit it to Badlands. Would anyone else be interested?


<a id="org298d9ba"></a>

## Weekly Questions

-   Who are the antagonists and protagonists? As a Marxist, Davis investigates the economic sources of power. Does he do a good job? How can we apply a similar technique to Boise?
-   What political movements rose up in LA and what can we learn from them?


<a id="org133ab5b"></a>

## Preface


<a id="org74cf77d"></a>

### Questions

“Regionally, we are no closer to real planning or coordination of housing, jobs and transportation than we were fifty years ago. There is much talk about ‘smart growth’ and ‘new urbanism,’ but, with few exceptions, the regional norms are still dumb sprawl and senile suburbanism. Some politicians still invoke magic bullets and sci-fi fixes, like 200 mph maglev trains, but Sacramento – which has recently siphoned off $2.5 billion in transportation funds to cover the budget deficit – is unable even to fill the potholes in our aging freeways. Southern California, as a result, is quickly turning into one huge angry parking lot. Congestion will inevitably drive away more jobs and business, while also fueling an ugly neo-Malthusian politics – already audible on the AM dial – of blaming immigrants (whose environmental footprint is actually the smallest) for declining physical and social mobility.” (p.xvi)

-   Is an auto-oriented development pattern more likely to induce "ugly neo-Malthusian politics"?
-   Is anyone telling a credible alternative story here? You already hear the blaming of California transplants for traffic and immobility on AM radio. It will only get more ugly as immigration inevitably becomes more diverse. Blaming outsiders is easy and compelling, how do we tell an equally interesting story?


<a id="orgefd686a"></a>

### Summary

The author reflects on the 16 years since the original publication. Ever the pessimist, he only sees tragedy. The Rodney King trial and riots never fully resolved, activist-turned politician Antonio Villaraigosa "emasculated", and a development pattern hell-bent on paving over the entire, ever unequal, region. This is the result of decades of governing by a "nationally unique coalition of Downtown business interests" (p.xi). 

It's clear at this point that the economic foundations of Boise and the Treasure Valley are much different from LA. We never had a significant manufacturing base to lose (I think?), we have yet to experience the same levels of minority migration, and we have yet to grow to a point where inequality can be clearly identified geographically (within the urban footprint).


<a id="orge1341e0"></a>

## Prologue


<a id="org64d1c55"></a>

### Questions

-   The tract-home suburbs are described as "stripped bare of nature and history", selling a mythical good life, and "pandering to a new, bergioning fear of the city". (p.4)
    -   What do they fear?
    -   To the extent the fear is legitimate, why is moving to the suburbs seen as the best option?
    -   What would a better solution?
-   What is the "Southern California Dream" (p.2)? Is it different from the American Dream?


<a id="org0972f8b"></a>

### Summary

Davis compares the utopian dreams of the YPSL in Llano del Rio to its current status as suburban hell, tract homes "stripped bare of nature and history". His writing is a barrage of depressing worst-case scenarios and hyperbole.


<a id="orge4eeffc"></a>

# Week 2 <span class="timestamp-wrapper"><span class="timestamp">&lt;2020-06-09 Tue&gt;</span></span>

**Reading:** Chapter 2: *Sunshine or Noir?*


<a id="orgfd49e4e"></a>

## *Sunshine or Noir?*


<a id="org3ac71df"></a>

## [Strong Towns: The More We Grow, the Poorer we Become](https://www.strongtowns.org/journal/2018/8/22/the-more-we-grow-the-poorer-we-become-td9nw)

This article gives an overview of the core themes of Strong Towns:


<a id="org6f8d000"></a>

### An Insolvency Crisis

The suburban pattern of development leaves municipal govts with more infrastructure than they can pay for. This leaves them constantly needing to raise taxes OR generate temporary revenue from growing outwards, but this only exacerbates the problem.

> "When a community builds a new road, that piece of infrastructure comes with a future obligation for maintenance. Local officials can estimate, with a good degree of precision, when that obligation will come due and roughly how much it will cost. In normal accounting terminology, that would be considered a future liability. In the magical world of municipal accounting, however, that road is labeled an asset&#x2026;
> 
> &#x2026;Local governments can’t function this way, not over the long run. They can’t take on more and more promises without generating enough wealth to meet those obligations—not without a reckoning."


<a id="orgb615234"></a>

### A Lack of Wealth

We continue to replace "Old and Blighted" developments with "Shiny and New" ones that are only better in clean appearance. When looked at closely, the old ones generated more wealth for the community, were more accessible for pedestrians, and were more economically diverse. 

I couldn't find property tax data for the 2012 parcels, but this transformation seems to be the type they are talking about.
Broadway Ave today: <https://goo.gl/maps/2nvqW3LtGQaNKnf7A>
2012: <https://goo.gl/maps/7CE5vFDuJK9yTLXL6>

1.  Some data

    Popeyes block
    
    <table border="2" cellspacing="0" cellpadding="6" rules="groups" frame="hsides">
    
    
    <colgroup>
    <col  class="org-left" />
    
    <col  class="org-right" />
    
    <col  class="org-right" />
    </colgroup>
    <thead>
    <tr>
    <th scope="col" class="org-left">Parcel</th>
    <th scope="col" class="org-right">Value</th>
    <th scope="col" class="org-right">Taxes</th>
    </tr>
    </thead>
    
    <tbody>
    <tr>
    <td class="org-left">R5315001770</td>
    <td class="org-right">162700</td>
    <td class="org-right">2124.08</td>
    </tr>
    
    
    <tr>
    <td class="org-left">R5315001802</td>
    <td class="org-right">1017200</td>
    <td class="org-right">12816.32</td>
    </tr>
    
    
    <tr>
    <td class="org-left">R5315001822</td>
    <td class="org-right">1406700</td>
    <td class="org-right">18551.82</td>
    </tr>
    
    
    <tr>
    <td class="org-left">R5315001842</td>
    <td class="org-right">920900</td>
    <td class="org-right">12146.36</td>
    </tr>
    </tbody>
    
    <tbody>
    <tr>
    <td class="org-left">&#xa0;</td>
    <td class="org-right">3507500</td>
    <td class="org-right">45638.58</td>
    </tr>
    </tbody>
    </table>
    
    ---
    
    Record World block
    
    <table border="2" cellspacing="0" cellpadding="6" rules="groups" frame="hsides">
    
    
    <colgroup>
    <col  class="org-left" />
    
    <col  class="org-right" />
    
    <col  class="org-right" />
    </colgroup>
    <thead>
    <tr>
    <th scope="col" class="org-left">Parcel</th>
    <th scope="col" class="org-right">Value</th>
    <th scope="col" class="org-right">Taxes</th>
    </tr>
    </thead>
    
    <tbody>
    <tr>
    <td class="org-left">R1955001555</td>
    <td class="org-right">212600</td>
    <td class="org-right">2630.56</td>
    </tr>
    
    
    <tr>
    <td class="org-left">R1955001561</td>
    <td class="org-right">327600</td>
    <td class="org-right">4052.22</td>
    </tr>
    
    
    <tr>
    <td class="org-left">R1955001566</td>
    <td class="org-right">168400</td>
    <td class="org-right">2158.00</td>
    </tr>
    
    
    <tr>
    <td class="org-left">R1955001570</td>
    <td class="org-right">129700</td>
    <td class="org-right">1609.11</td>
    </tr>
    
    
    <tr>
    <td class="org-left">R1955001575</td>
    <td class="org-right">197600</td>
    <td class="org-right">2429.78</td>
    </tr>
    
    
    <tr>
    <td class="org-left">R1955001580</td>
    <td class="org-right">321700</td>
    <td class="org-right">4115.00</td>
    </tr>
    
    
    <tr>
    <td class="org-left">R1955001585</td>
    <td class="org-right">179100</td>
    <td class="org-right">2289.64</td>
    </tr>
    
    
    <tr>
    <td class="org-left">R1955001602</td>
    <td class="org-right">568500</td>
    <td class="org-right">9853.20</td>
    </tr>
    </tbody>
    
    <tbody>
    <tr>
    <td class="org-left">&#xa0;</td>
    <td class="org-right">4210400</td>
    <td class="org-right">29137.51</td>
    </tr>
    </tbody>
    </table>
    
    Idaho Pizza Company block
    
    <table border="2" cellspacing="0" cellpadding="6" rules="groups" frame="hsides">
    
    
    <colgroup>
    <col  class="org-left" />
    
    <col  class="org-right" />
    
    <col  class="org-right" />
    </colgroup>
    <thead>
    <tr>
    <th scope="col" class="org-left">Parcel</th>
    <th scope="col" class="org-right">Value</th>
    <th scope="col" class="org-right">Taxes</th>
    </tr>
    </thead>
    
    <tbody>
    <tr>
    <td class="org-left">R1955001472</td>
    <td class="org-right">696200</td>
    <td class="org-right">8426.78</td>
    </tr>
    
    
    <tr>
    <td class="org-left">R1955001480</td>
    <td class="org-right">109800</td>
    <td class="org-right">1148.48</td>
    </tr>
    
    
    <tr>
    <td class="org-left">R1955001485</td>
    <td class="org-right">318600</td>
    <td class="org-right">3642.42</td>
    </tr>
    
    
    <tr>
    <td class="org-left">R1955001495</td>
    <td class="org-right">648000</td>
    <td class="org-right">8798.54</td>
    </tr>
    </tbody>
    
    <tbody>
    <tr>
    <td class="org-left">&#xa0;</td>
    <td class="org-right">1772600</td>
    <td class="org-right">22016.22</td>
    </tr>
    </tbody>
    </table>
    
    Jalapeño's Block
    
    <table border="2" cellspacing="0" cellpadding="6" rules="groups" frame="hsides">
    
    
    <colgroup>
    <col  class="org-left" />
    
    <col  class="org-right" />
    
    <col  class="org-right" />
    </colgroup>
    <thead>
    <tr>
    <th scope="col" class="org-left">Parcel</th>
    <th scope="col" class="org-right">Value</th>
    <th scope="col" class="org-right">Taxes</th>
    </tr>
    </thead>
    
    <tbody>
    <tr>
    <td class="org-left">R1955001095</td>
    <td class="org-right">1582600</td>
    <td class="org-right">23528.86</td>
    </tr>
    
    
    <tr>
    <td class="org-left">R1955001076</td>
    <td class="org-right">996800</td>
    <td class="org-right">12569.68</td>
    </tr>
    </tbody>
    
    <tbody>
    <tr>
    <td class="org-left">&#xa0;</td>
    <td class="org-right">2579400</td>
    <td class="org-right">36098.54</td>
    </tr>
    </tbody>
    </table>
    
    Quinn's block
    
    <table border="2" cellspacing="0" cellpadding="6" rules="groups" frame="hsides">
    
    
    <colgroup>
    <col  class="org-left" />
    
    <col  class="org-right" />
    
    <col  class="org-right" />
    </colgroup>
    <thead>
    <tr>
    <th scope="col" class="org-left">Parcel</th>
    <th scope="col" class="org-right">Value</th>
    <th scope="col" class="org-right">Taxes</th>
    </tr>
    </thead>
    
    <tbody>
    <tr>
    <td class="org-left">R3210000100</td>
    <td class="org-right">240300</td>
    <td class="org-right">3038.98</td>
    </tr>
    
    
    <tr>
    <td class="org-left">R3210000111</td>
    <td class="org-right">177500</td>
    <td class="org-right">2247.58</td>
    </tr>
    
    
    <tr>
    <td class="org-left">R8248000018</td>
    <td class="org-right">560600</td>
    <td class="org-right">9150.00</td>
    </tr>
    
    
    <tr>
    <td class="org-left">R8248000005</td>
    <td class="org-right">411900</td>
    <td class="org-right">5206.94</td>
    </tr>
    </tbody>
    
    <tbody>
    <tr>
    <td class="org-left">&#xa0;</td>
    <td class="org-right">1390300</td>
    <td class="org-right">19643.5</td>
    </tr>
    </tbody>
    </table>
    
    Vista McDonald's
    
    <table border="2" cellspacing="0" cellpadding="6" rules="groups" frame="hsides">
    
    
    <colgroup>
    <col  class="org-left" />
    
    <col  class="org-right" />
    
    <col  class="org-right" />
    </colgroup>
    <thead>
    <tr>
    <th scope="col" class="org-left">Parcel</th>
    <th scope="col" class="org-right">Value</th>
    <th scope="col" class="org-right">Taxes</th>
    </tr>
    </thead>
    
    <tbody>
    <tr>
    <td class="org-left">R8112007300</td>
    <td class="org-right">1686300</td>
    <td class="org-right">16947.98</td>
    </tr>
    </tbody>
    </table>
    
    None of these numbers are as egregious as the Strong Towns example, when accounting for an expected boost in property value with newer buildings, they are fairly consistent in tax revenue/acre. I would assume the older style employs more people (infrastructure cost/person) and, given the density of business, is more stable over the long run. It is also more pedestrian friendly and encourages less car use (car expenses limit other economic activity/consumption)


<a id="orgd5182ce"></a>

### Simple Math

How would we use public data to calculate private wealth/public investment ratio?
Developers aren't taxed more for putting buildings in places where building infrastructure is difficult (like pumping water uphill). This leads to bad design decisions.


<a id="org6c8f3c9"></a>

### Strong Towns Approach

> "To make our communities not just solvent but financially strong and resilient, we must increase our wealth without increasing—and perhaps even by decreasing—our expenses. Instead of focusing on new growth, we need to obsess about making more productive use of that which we’ve already built&#x2026;
> 
> Here is our approach:
> 
> -   Rely on small, incremental investments (little bets) instead of large, transformative projects.
> -   Emphasize resiliency of result over efficiency of execution.
> -   Design to adapt to feedback.
> -   Inspire by bottom-up action (chaotic but smart) and not top-down systems (orderly but dumb).
> -   Seek to conduct as much of life as possible at a personal scale.
> -   Obsess about accounting for revenues, expenses, assets, and long-term liabilities (do the math).
> 
> "


<a id="org56e09c7"></a>

# Supplemental

Send suggestions to <mailto:mail@andrewchumich.com>


<a id="org0bb77eb"></a>

## Books

-   [Set the Night on Fire: LA in the 60s](https://www.versobooks.com/books/3164-set-the-night-on-fire)


<a id="orgf6ac37f"></a>

## Movies

-   LA 92 (should be on Netflix)
-   Going Clear (HBO)


<a id="org1a1e09e"></a>

# Misc quotes

-   <https://www.facebook.com/code3to1/posts/112177920509921>
    -   Boise Mayor McLean recently released her transition team's report on what her term will look like. It is full of issues that destroyed California, Oregon, Washington, and Colorado to name a few states. Our retired officers and fire fighters have seen this happen before. They moved to Idaho based on the results of those failed policies. This will affect all of Idaho, not just Boise. Please share our open letter to Mayor McLean so that we can avoid the mistakes of places like California.
    -   **Greg Gritsch** I am a new resident to the Treasure Valley coming from Oregon last September. I left Oregon after finishing 39 years in law enforcement. The primary reason I left Oregon after 50 years was the idiotic and stupid policies of the left that has run Oregon for many years. The exact policies that have been recommended to the Boise mayor are the reasons Oregon, Washington and California are in the ruinous state they are in. Those "suggestions" are exactly what Boise and this state does not need. Progressive pablum has not worked in any of the major cities of our nation. I came here because Idaho has the values that are lacking in so many of the democratic run states and cities. My motto is, "I came here to join you-not to change you".
    -   **Travis Rosenberry** Thank you for posting this. My family moved here from CA in 1976 and it was a great place to grow up. Many of my relatives are still trapped in CA and the stuff in this letter seems to describe the insanity my family is suffering under in CA. I frequently meet families trying to escape such depressed places and I do everything I can to help them settle into a new life in the beautiful Treasure Valley. Please continue to share your experiences from these failed states so that this can remain the amazing place that it has been.


<a id="orgf76a1a3"></a>

# Links (Articles, videos, etc.)

-   [Strong Towns: The More We Grow, the Poorer We Become](https://www.strongtowns.org/journal/2018/8/22/the-more-we-grow-the-poorer-we-become-td9nw)
-   [Meridian reconsiders its growth](https://www.idahostatesman.com/news/local/community/west-ada/article241134126.html)
-   [Treasure Valley faces regionwide problems. Its fragmented governments don’t solve them](https://www.idahostatesman.com/news/local/community/west-ada/article234019817.html)
-   [A 2007 report predicted trouble unless Valley leaders disciplined growth. They didn’t.](https://www.idahostatesman.com/news/business/article233580167.html)
-   <http://linguafranca.mirror.theinfo.org/9709/davis.html>
-   [Natural disaster lecture](https://www.youtube.com/watch?v=evJpgKQ6YWU)
-  [Meridian reconsiders its growth](https://www.idahostatesman.com/news/local/community/west-ada/article241134126.html)
