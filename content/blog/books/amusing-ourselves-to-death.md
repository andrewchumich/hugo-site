+++
title = "Amusing Ourselves to Death"
date = "2023-08-17"
lastmod = "2023-08-17"
categories = ["books"]
tags = ["Neil Postman", "Log Off"]
+++

Can you perform romantic poetry using flag semaphore? Possibly. Can an electric guitar tell a
science fiction tale? Despite Rush's best efforts, not very well. Can a television convey political philosophy? "With great inconvenience" says Neil Postman. In *Amusing Ourselves to Death*, he makes his case against the TV. 

##### The Age of Exposition

A society's primary form of communication shapes our conception of truth. It prioritizes certain types of truth-telling over others. An oral society may place heavy importance on proverbs and sayings, valuing rhetoric and flourish. This is not credulity, the tradition and ornamentation *are* the evidence. "Truth does not, and never has, come unadorned" (p.22). In a written-language society, truth is found and perceived differently. The written culture values clarity, attentiveness, "a sophisticated ability to think conceptually, deductively, and sequentially...an abhorrence of contradiction."  (p.63).

Written language generally requires its author make refutable claims, to pluck scattered and mismatching thoughts
from their mind and generate a clear train of thought. I feel this requirement now, as I write
this essay. Ideas that feel known and understood floating in my mind are often exposed to be faulty once written down, forcing me to be self-reflective, critical, and adaptable, to be *sharp*.

##### The Age of Show Business

If the printed word gives "priority to the objective, rational use of the mind" (p.51) and encourages readers to follow long arguments, connect ideas, process generalizations, and discover disordered thinking, what does the TV give priority to? Despite the widespread phenomenon of "binge-watching" television (phone in hand, of course), there can be no doubting the medium's tendency to diminish our attention spans. 

Perhaps more accurately, it is not the length of attention that has shortened, but the length of any given single "context" of thought. The TV brings information from all over the world almost instantaneously into our consciousness, but the demands on TV's entertainment value means that this information is likely unrelated to what came before and what will come after. While being able to switch contexts is a positive human trait, the rapidity of context changes, Postman argues, influences our understanding of the world:

> The fundamental assumption of [the world of TV] is not coherence but discontinuity. And in a world of discontinuities, contradiction is useless as a test of truth or merit, because contradiction does not exist. My point is that we are by now so thoroughly adjusted to the "Now...this" world of news...that all assumptions of coherence have vanished. (p.110)

This lack of continuity pairs with TV's absurdly high "information-action ratio" (p.68). It gives plenty of information, but so little of it can be meaningfully acted upon. 

## "What is to be Done?"

Though arguments against TV and news-as-entertainment are not new, Postman's perspective feels fresh.
Rather than a call to democratize electronic media or remove its profit motive, he argues against the media in its *form*. As long
as the TV dominates our way of knowing, we will never combine ideas with discipline and action on a scale large enough to be *democratic*.

How does all this translate to the internet? Despite the internet's early promise of merging mass political knowledge with direct communication and bypassing the TV's unidirectional flow of information, it has so far only produced [moribund attempts at change](https://vincentbevins.com/book2/). It seems, to me, that the internet suffers from the same rapid context switching and high information-action ratio problems of the TV. When digital activists finally meet in real life to press their ideas against the grindstone of political struggle, their on-screen sharpness and potency are usually revealed to be illusory.

Im my mind, we have only one meaningful choice: log off.
