+++
title = "The Republic for Which it Stands"
date = "2021-01-30"
categories = ["books"]
tags = ["Richard White", "The Republic for Which it Stands", "history"]
+++


Richard White's *The Republic for Which it Stands* is an intimidatingly large history. It which takes us from Andrew Johnson, who argued to Frederick Douglass' face that poor whites were the real victims of slavery, through the parade of bearded Presidents, and all the way to William McKinley. This was an America unable to decide what it stood for. The republican dream of independent producers was constantly threatened, but no alternative could take hold. While freemen suffered in the South, Native Americans were pushed from their lands, businessmen built railroads to nowhere, and wage labor became a permanent status. 

The end of the Civil War held great promise, especially in the eyes of the newly freed slaves. Had they and their Radical Republican allies had their way, vast swaths of southern plantations would have been redistributed. But Lincoln was dead and Andrew Johnson had little sympathy for the freedmen, removing Union troops from the South and stripping the Freedmen's Bureau of power. Reconstruction required force, but Johnson refused to provide it, claiming to be afraid of a future "war of races", but in reality supporting his side of the existing one. The opposite happened on the frontier. Johnson and his successors felt justified in sending troops to crush native resistance, believing they were protecting American homes.

For reasons that are still unclear to me, railroads began taking on staggering amounts of debt. Somehow the productive capacity of American industry along with government land grants led to heavy railroad speculation. "Railroads through deserts&#x2013;beginning nowhere and ending nowhere" (261) should remind modern readers of driving through the exurbs in 2007. Periods of rapid expansion and dramatic crashes were primarily driven by the railroads. Unlike the frontier of our imaginations, the railroad came before the covered wagon.

Lincoln and the Republican Party envisioned an America where waged labor was merely a stepping stone towards property ownership and indepencence. Paradoxically, the productivity of the American economy challenged this vision. Better farming techniques required less labor and industrial innovation threatened to make the small manufacturing shop obsolete and cheap American grain exports helped drive immigration. Though neither of these trends were totalizing, wage labor became a permanent status and farmers were constantly on the verge of bankruptcy. They attempted to organize. From the Knights of Labor and AFL, to the Farmers' Alliance and Progressive Party, it might have seemed at the time that Marx's proletariat was being born, but revolution was only on the minds of a small minority. 

Richard White paints the end of the 19th Century with both broad and narrow brushes. There's no way I'll retain all the information in *The Republic for Which it Stands*, but a period of US history that looked dark and blurry in my mind is now filled with color and orientation. 

