+++
date = "2021-09-06"
title = "Marx and the Metaverse"
authors = ["andrew-chumich"]
draft = true
+++

Marx thought that getting rid of religion would allow humans develop a true sense of the world around them. With this
knowledge, we would organize to change the world for the better.

> The abolition of religion as the illusory happiness of the people is the demand for their real happiness. To call on them to give up their illusions about their condition is to call on them to give up a condition that requires illusions.

- *A Contribution to the Critique of Hegel’s Philosophy of Right* - Karl Marx

Religious belief and practice has been on a slow and steady decline. But we are no closer to having a singular, rational way of understanding the world. If we have "plucked the imaginary flowers on the chain", why have we yet to throw it off? 

I have two (non exclusive) answers:
1. Religion was the wrong target. Though it fosters some illusions, it actually contains real truths. If religion is the "heart of a heartless world", we have yet to find a suitable heart replacement. Or it is a heart we should keep.
2. Capitalism has developed new means of creating illusions.

This post is mostly concerned with answer 2. 

[Mark Zuckerberg and Facebook recently announced](https://www.theverge.com/22588022/mark-zuckerberg-facebook-ceo-metaverse-interview) that the company was investing heavily in what is being called the "metaverse". Unlike the current paradigm of separate apps for different uses, the metaverse will be a ecosystem of connected apps and technologies. With the help of VR, AR, and digital currencies, users will presumably enter a realm that incorporates office work, social media, and entertainment. 

>You can think about the metaverse as an embodied internet, where instead of just viewing content — you are in it. And you feel present with other people as if you were in other places.

>What I’m excited about is helping people deliver and experience a much stronger sense of presence with the people they care about, the people they work with, the places they want to be.

It's been clear for a while that the internet as it exists today can greatly distort people's sense of reality. Experiences and knowledge found online often do not translate well to the real world. Political "movements" founded online rarely turn into effective organizations. (NEED MORE EXAMPLES)

The metaverse will only maginify these issues. 

Who knows how successful these platforms will be, but there's a lot of money behind them. 