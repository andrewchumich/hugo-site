+++
title="McLean vs. Masterson"
date=2023-08-18T11:12:31-06:00
authors=["andrew-chumich"]
tag=["boise"]
draft=true
+++

Lauren McLean is running for her second term as Boise's mayor. Her only challenger so far is former police chief Mike Masterson. 

To my disappointment (though without surprise), neither candidate has released substantial positions papers or memos on policy. McLean's website has a page with nice graphics listing some of her accomplishments in her first term. Some of the items are meaningful, but they are mostly worded in the same way as a recent college grad's resume ("Proficient in Microsoft Excel"). 

Masterson's has an issues page with 3 sections:

- Housing & Neighborhoods
- Public Safety
- Solving Homelessness

None of them rise above platitudes like "focus on smart growth while protecting our neighborhoods and our voices." and focusing on "community solutions" to homelessness. 


