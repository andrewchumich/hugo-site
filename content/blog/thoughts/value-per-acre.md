+++
date = "2021-11-12"
title = "Value Per Acre in Ada County"
authors = ["andrew-chumich"]
draft = false
+++

Inspired by [Strong Towns](https://www.strongtowns.org/journal/2017/1/29/the-cost-of-auto-orientation-rerun), I wanted to do a value-per-acre in Ada County. I started off just picking assorted parcel's from the [Ada County Assessor interactive map](www.adacountyassessor.org/adamaps/):

**Boise:**

| Location | Value/Acre |
|-------------|---------|
| Winco (Downtown) | $1.306 million/acre |
| Trader Joe's | $4.941 million/acre |
| Whiskey Bar | $5.6 million/acre |
| Costco (Cole) | $797k/acre |


These three from the "blighted"-looking parcels near Boise Ave. and Broadway


| Location | Value/Acre |
|-------------|---------|
| Coble Company | $2.557 million/acre |
| Thompsons INC | $3.304 million/acre |
| Kiddo Holdings LLC | $2.671 million/acre |


All better than downtown Winco and Costco!


**Moving on to Meridian:**

| Location | Value/Acre |
|-------------|---------|
| Village (doesn't include a bunch of parking) | $6.4 million/acre | 
| Big Als | $2.279 million/acre | 
| Costco (Chinden/Ten Mile) | $1.4 million/acre | 
| Walmart (State/Gary) | $1.3 million/acre | 
| 105 E IDAHO AVE MERIDIAN | $4.886 million/acre | 
| 127 E IDAHO AVE MERIDIAN | $3.318 million/acre | 
| 136 E IDAHO AVE MERIDIAN | $5.13 million/acre | 
| Epi's | $1.5 million/acre | 
| El Tenampa | $2.745 million/acre | 


Properties in Downtown Meridian are much more valuable than most of the new and shiny megastores. 

----

Okay, clicking on the map is dumb, where's data? I downloaded the "Ada County Parcel polygons" file from https://adacounty.id.gov/Assessor/Land-Records/Ada-County-Assessor-Data/


It's a shapefile! Very nice.

{{< figure src="/blog/thoughts/images/file-screenshot.png" title="finder screenshot" >}}

Opening it up in Google Earth Pro, it can't handle the whole file, but it seems it has all the data we want (acres and total value mostly)


{{< figure src="/blog/thoughts/images/gmaps-shapefile.png" title="google earth screenshot" >}}


It would be nice to have all this data in a database. I'll use `ogr2ogr` to import the data to a PostGIS database:

`ogr2ogr PG:"host=localhost port=5432 dbname=YOUR_DB_NAME user=YOUR_DB_USER password=YOUR_DB_PASSWORD" -nlt GEOMETRY parcelspublic.shp -skipfailures -lco precision=NO`


```SQL
-- Remove parcels with 0 acres to avoid divide-by-zero error
DELETE FROM parcelspublic WHERE acres = 0;

-- Add a value_per_acre generated column just to be handy
-- Need Postgres 12 and up
ALTER TABLE parcelspublic
ADD COLUMN value_per_acre float8 GENERATED ALWAYS AS (totalvalue / acres) STORED;
```

The first query I ran was to find the most valuable properties. Unsurprisingly, they are almost all downtown:

{{< figure src="/blog/thoughts/images/most-value.png" title="most valuable properties" >}}


There's only so much you can do with just the database. I'd rather have a map view. Let's connect to it with QGIS


{{< figure src="/blog/thoughts/images/qgis-1.png" title="qgis screenshot" >}}

Nice!

Let's also filter only the commercial properties and add a satellite tile source:

`http://mt0.google.com/vt/lyrs=y&hl=en&x={x}&y={y}&z={z}`

{{< figure src="/blog/thoughts/images/gmaps-and-commercial.png" title="qgis screenshot 2" >}}


Next step: color code by value_per_acre:

{{< figure src="/blog/thoughts/images/ada-value-per-acre.png" title="qgis screenshot 3" >}}


**Downtown**

{{< figure src="/blog/thoughts/images/downtown.png" title="Downtown Boise" >}}

**West Boise**

{{< figure src="/blog/thoughts/images/west-boise.png" title="West Boise" >}}


**Meridian**

{{< figure src="/blog/thoughts/images/meridian.png" title="Meridian" >}}

**North Meridian & Eagle**

{{< figure src="/blog/thoughts/images/north-meridian-eagle.png" title="North Meridian & Eagle" >}}

**Texas?**

{{< figure src="/blog/thoughts/images/apple-st.png" title="Dairy Queen saves the day" >}}


Not sure where to go with this next but it was fun. 