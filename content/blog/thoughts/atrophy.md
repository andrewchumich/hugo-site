---
title: "Atrophy"
date: 2023-10-02T13:00:00-06:00
draft: false
categories: []
tags: ["Log Off"]
---

The democratic body is in severe condition, in both flesh and mind. After more than a decade of disciplined training in the 1930s and 40s, we were a toned and capable athlete. While not yet at our apex, we could still strike fear in the eyes of most of the competition. But complacency, [changes to the rulebook](https://en.wikipedia.org/wiki/Taft%E2%80%93Hartley_Act), and (most importantly) [distraction]({{< relref "/blog/books/amusing-ourselves-to-death.md" >}}), veered us away from a hall-of-fame career. There was no freak accident, it didn't happen overnight. But slowly, over the decades, we lost our strength. We exchanged gumption for timidity, championship dreams for a quick win, focus for short & narrow sight. We warped our pleasure principle, no longer finding meaning in the toil required to achieve greatness, we instead began to seek quick, libidinal comforts. Once strong, our muscles atrophied. Once the epitome of coordination, our nerves lost connections. We are now weak and clumsy.


None of this is unrecoverable. With a positive attitude, patience, and good rehab plan, we could certainly return to form. But there is another problem. Our driving pursuit of immediate comfort led us to embrace new technologies of virtual reality. We aren't entirely to blame. The sales pitch was tantalizing and misleading. This digital life, we were told, would inspire us to live better lives. It would connect us without barriers. We would learn the secrets to a faster recovery that "doctors don't want you to know"! But the sad reality, with the wisdom of retrospect, is that we are further than ever from good health.


Some days we take off the headset and catch a glimpse of the real situation, but our mind distorts it. Instead of a long road to recovery, we envision a miraculous comeback. So we put on our old uniform, open the door, and head straight to [this year's championship](https://en.wikipedia.org/wiki/Bernie_Sanders_2016_presidential_campaign). Of course the attempt is hopeless, but not *worthless*. It's such failures we need to un-warp our vision. In that moment of defeat we must take some silent time to reflect on where we are. We can't run, we can barely stand. We can't throw, we can barely lift the shot.


If we really want to get back into fighting shape, we have to take the small steps. Re-learn fundamental techniques, slowly build muscle, and accustom ourselves (again) to the pleasure of long-term discipline. Log off, and stick to the training plan.