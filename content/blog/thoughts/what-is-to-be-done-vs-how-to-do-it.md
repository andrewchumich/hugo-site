---
title: "What Is to Be Done vs How to Do It"
date: 2023-09-22T13:00:00-06:00
draft: false
categories: []
tags: ["Organizing"]
---

We've forgotten how to build membership organizations. Yet the success of democratic society will depend on them. 

A membership organization is built by:
- Determining the constituency/class it will represent
- Finding (and convincing) influential people in the constituency to recruit members
- Asking new members to sacrifice time, talent, and money towards the organization

I have seen a few attempts at organizing in Boise and they generally failed these steps. The main exceptions are the Leninists and Reclaim Idaho (I'll discuss them more below). The Leninists' failure is mostly due to their revolutionary orientation which tends to attract weirdos and cranks. Before we can consider ideology, we must re-learn how to build an organization. 

I've been reading a [history of the Non-Partisan League](https://press.uchicago.edu/ucp/books/book/chicago/I/bo21028306.html) and been struck by their simple tactics. They would drive around, knock on doors, walk up to farmers in the field, and convince them to give $16 (in 1915 money) for a year-long membership to the organization. Members received the monthly League publication and could vote in the regular elections. Pretty soon they had enough money to staff an office and expand across the state. These were not new tactics, but old ones employed at the opportune time.

The most successful political organization in Idaho is Reclaim Idaho. They correctly recognize that democratic political power comes from building long-term relationships. They have built a large *voluntary* association across the state, capable of gathering hundreds of thousands of signatures, getting out the vote, and pressuring the state government to uphold the results of their ballot initiatives. Their success shows the right path, but to pack a larger punch, we need *membership, class-based organizations*. For that we need to start small and parochial. 

Here's an idea. Boise's North End (my neighborhood) has about 4000 renters. If a group could convince 20% of those to pay $10/month, the organization would have a budget of $8000/month. That's enough to pay 1 full time and 2 part-time writers/organizers. Maybe it would be difficult, but it's well within the realm of possibility.

