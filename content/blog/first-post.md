+++
date = "2018-04-04T17:44:21+00:00"
title = "D3 Presentation"
authors = ["andrew-chumich"]
draft = true
+++

My presentation at the [Boise Front-end Development](https://www.meetup.com/frontend-devs/events/249359171/) meetup.

[Download](/presentations/d3_presentation.key)
