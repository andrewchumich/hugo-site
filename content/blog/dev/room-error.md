+++
title = "Random Dev Notes (Room ~@Relation~ Dao Error)"
date = "2020-05-27"
categories = ["dev"]
tags = ["android", "room"]
+++

Apparently with Room on Android, you can't use `@Insert` Dao methods with a POJO. It throws an unhelpful error that took two entire days to figure out. I'm writing this so I never forget and to maybe help someone out who googles it next.

Say you have a model and POJO like:
```kotlin
    @Entity(...)
    data class User(
      val id: String,
      val name: String
    )
    
    @Entity(...)
    data class Pet(
      val id: String,
      val type: String,
      val name: String,
      val userId: String
    )
    
    data class UserWithPets(
      val id: String,
      val name: String,
      @Relation(
        parentColumn = "id",
        entityColumn = "userId"
      )
      val pets: List<Pet>
    )
```

If you write a Dao method like:
```kotlin
    @Insert(entity = User)
    fun save(userWithPets: UserWithPets)
```

You will get the error: `Partial entities cannot have relations.`

If you do a web search, all you get is some [source code](https://android.googlesource.com/platform/frameworks/support/+/02684f53abe601dc8b863638bf4ae38a3d54c029/room/compiler/src/main/kotlin/androidx/room/processor/ShortcutMethodProcessor.kt) (line 110) which is hardly helpful. It has a `// TODO: Support Pojos with relations.`. Maybe this will get added soon?

