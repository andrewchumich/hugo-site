+++
title = "Notes: Can't Get You Out of My Head"
date = "2021-03-11"
categories = ["movies", "documentaries"]
tags = ["Adam Curtis", "history"]
+++

[*Part 1 --- Bloodshed on Wolf
Mountain*](https://thoughtmaybe.com/cant-get-you-out-of-my-head/#top)

**Old Power**

As the British Empire declined, people were unsure how to move forward.
Stories of violence and abuse confused and terrified those at home.
Afraid of those they colonized, the British middle class revealed their
racism as many began moving to the island. One of these, Michael de
Freitas, was surprised at how it differed from his imagination. As a
subject of the Empire, he was always told England was his homeland, but
he found himself an outsider. British men were also struggling to
maintain their powerful status in the home and at work.

**China, new power, and individualism**

Though China looked like an example of collectivism, some of those at
the top suffered from the same paranoia and fear as those in Europe and
the US. Curtis looks at the life of Mao's wife Jiang Qing, who went from
a striving actress to having the ear of the Supreme Leader of the CCP.
Vengeful and paranoid, she used her power to get revenge on those who
she believed wronged her. It's hard to tell where Curtis is going with
her story. She seems like a crazy lady, but there must be more to it.

**US, fear of the old world, and patterns**

Americans know paranoia well. Immigrants were fearful of being followed
by whatever drove them from Europe. The fear of savage Native Americans
justified their slaughter. As postwar Americans settled into their new
suburban life, they were as unsettled as ever. Isolated from a community
that could give their lives shape and meaning, they were susceptible to
fearful conspiracies. The John Birch Society spread rumors of Eisenhower
being a foreign agent, anticommunism was rampant. Out of this grew a
counterculture, just as willing to believe in fantastic tales. Less
interested in using the power of reason to understand the world,
Americans looked for patterns to confirm suspicions. While the CIA was
engaging in real conspiracies like MKUltra, American's latent paranoia
magnified and distorted the dark forces influencing our lives.

[*Part 2 --- Shooting and F\*\*king are the Same
Thing*](https://thoughtmaybe.com/cant-get-you-out-of-my-head/#top)

**China and Communist Witch Hunts**

Jiang Qing leads the youth towards the Cultural Revolution. The Chinese
Revolution failed to bring about the new working class. Those resisting
needed to be rooted out. The "little devils of youth" were organized
into the Red Guard. Excused from school, they were tasked with finding
the traitors to the revolution. Teachers, parents, and party
administrators were all at risk of imprisonment, execution, and public
humiliation if they refused to admit their thought crimes. Curtis
asserts that the real sources of power hadn't changed with the
revolution and they were trying to force ideals onto reality (I think
that's what he was saying).

**Cops are the Best Organizers**

The Black Panthers catalyzed the radicals into fighting for Black Power.
Not as disciplined as they seemed from the outside, chapters were easily
infiltrated by informants. Alice Faye Williams (later Afeni Shakur)
joined the Panthers, but quickly became suspicious of some members. She
was not able to convince others of her suspicions. There turned out to
be 3 informants in the chapter. Unsurprisingly, it was their work that
kept the chapter functioning, the other members off "doing their own
thing" most of the time. The informants pushed the hardest for violent
actions and were able to get enough evidence to put most of the chapter
on trial. After an impressive cross-examination by Shakur, all were
acquitted, but the wind in the sails of the Black Power movement had
died out.

**Failure, Pessimism, and Branding**

The failure of those who tried to take power in the 60s often differed
in style, but the substance was the same. As much as they tried to
locate power, they never seemed to find it. Neither the "propaganda of
the deed" nor respectable organization could galvanize the dormant
working class. For Europe and America, power was being drained from
domestic factories and workplaces and into the hands of international
finance. The combination of psychology and economics was growing
powerful and confident. Through global trade and clever marketing, the
working class could be kept in a dream world, where the market bends to
their individuality. The aesthetics of the revolutionaries could be
commodified and adopted by the growing managerial classes. The
ex-revolutionaries seemed to have a way out of their failures: monetize
their own brand :)

[*Part 3 --- Money Changes
Everything*](https://thoughtmaybe.com/cant-get-you-out-of-my-head/#top)

**Draining Power With Oil**

The American working class, for a brief moment, had power. They could
halt the factory, stop the coal trains, and bargain for a better life.
The shell of a welfare state was built by their power and threat to
exercise it. But machines were coming to make humans redundant and they
lost power rapidly. And coal began to be replaced with oil from around
the world, especially the Middle East. More machines and fewer points of
failure. The working class was losing power. Many stayed where they
were, becoming poor and hopeless. Others rode the economic wave to the
suburbs, into a different form of isolation. The Sacklers capitalized on
the resulting fear and anxiety, producing Valium which promised to cure
the lurking dread, but often compounded it.

**China & USSR, two failed revolutions, two different outcomes**

As Mao neared death, a power struggle began. Jiang Qing and the Gang of
Four lost out to Deng Xiaoping. She turned out to be a pawn in a
struggle for power, not a true holder of it. Xiaoping, recognizing the
failure of their economic system, brought market reforms. A state
directed capitalism could undercut production and shift the balance of
power away from the West. With banks now willing to lend for
consumption, the working classes of the West could simultaneously lose
economic power and continue to spend. The other red revolution in the
Soviet Union was also failing. True believers were betrayed while
dissidents were vindicated, though even they had no vision of the
future. When the Soviet state broke down, they couldn't make the same
turn as China. Instead, greed and cynicism led them into the 21st
century.

**Climate Change, Instability, and New Power**

While ideology loosened its grip on the world, uncertainty seemed to
come from every direction. Climate scientists started to warn of
unstable systems and catastrophe. Fossil fuels created wealth and
potential destruction. Conspiracies which seemed fringe, began to make
sense to more people. The Church Committee validated their existence,
and paranoid pattern searchers dreamed up new ones. The power to stop
the American in economy, which was held by the working class in the
1940s, had been transferred to the Saudis and banks. The loss of power
leads to fear and delusion, and this time, there was a market for it.

[*Part 4 --- But What If the People Are
Stupid?*](https://thoughtmaybe.com/cant-get-you-out-of-my-head/#top)

**New Bureaucracy and Self Actualization**

Radical individualism had no power, but that didn't stop many from
trying. Those with Left ideological allegiances found themselves
condemning Vietnamese refugees while radical nonpartisans couldn't
distinguish a Mujahideen from Martin Luther King Jr. Live Aid may have
killed as many as it saved, but at least it shot Bono into fame.
Deindustrialization and the consumer economy led people on a search for
their "true selves" which rarely satisfied. Meanwhile, politicians who
once drew their power from the working masses found themselves alone in
the rain. Fortunately for them, the financial and managerial classes had
umbrellas. This "new bureaucracy" established "non majoritarian
institutions", promising stability.

**Tiananmen Square, the Chinese Occupy**

Jiang Qing was sentenced to death, but called for those remaining to
rebel. Individualism began to seep into China. Deng Xiaoping\'s
short-lived democratic experiment helped give rise to a coherent
dissident movement. Led by students of course. After the death of a
loved party member, students occupied Tiananmen Square. The students
were justly angry, but with no larger story to submit themselves to, how
could they hope to challenge the power of Beijing? Though an honorable
attempt to ignite a wave of democracy, the students were easily crushed
by the military. Many of the Chinese people (it seems), were
uninterested in the pretense of democracy, but were perfectly accepting
of the power of finance and the rule of "money and connections".

**"Who are you going to believe? This boy you are knowing your whole
life? Or this boy you are just meeting, who says 'the world economy' a
thousand times?" - Isabella Parigi**

After Russia's feeble attempts at democracy descended into corruption,
oligarchy, and violence, some sought a new grand story. Those left
behind by the "new bureaucracy" tried to create new political
institutions, but were just as weak as the left radicals. Limonov
founded the National Bolsheviks, galvanizing many disaffected youths.
This emerging nationalism, however weak, is the source of current
conservatism. It may seem laughable to hear someone defend the British
Empire as defending against a "tyranny of the majority", but in the age
of global finance, regional capitalists can believably paint themselves
the honorable underdog. Tony Blair sells out the working class.

[*Part 5 --- The Lordly
Ones*](https://thoughtmaybe.com/cant-get-you-out-of-my-head/#top)

**Imperial Fears and Shaping the World With Dreams**

As the British Empire continued to disintegrate, England became fearful.
Attempts to reckon with the violence and terror committed by their
country, led not to reconciliation, but paranoia. Would those they
subjugated, like China, come back for revenge? The middle classes were
most susceptible. They felt surrounded by alien forces. The working
class could be angry and violent, the bourgeois seemed corrupt and
lacking loyalty. Out of this, they crafted a new idealized Britain.
Drawing from supposed rural traditions, they dreamt of a natural order
based in the countryside. Unspoiled by urban life and global capital,
but advanced enough to be comfortable. Where democratic solutions were
simple and lives were lived undisturbed by the complexities of a global
economy (just like the American frontier). This dream also powered
America, but with its signature racial character (not that Britain
lacked racism). The second KKK represented the return of a harmonious
natural order. The middle classes retreated into the suburbs, looking
for this mythical and society, and destroying whatever real bucolic life
actually remained.

Given the power and reach of their economies and militaries, it's no
surprise that these dreams shaped their imperialism. When dividing the
Middle East after the Ottoman Empire, Iraq was given to the Sheikhs,
rural tribal leaders who seemed to embody that idealized natural order.
Ignoring the complex society of the urban areas, Britain left Iraq with
an unstable power structure. The US picked up where Britain left off.
Oil and computer materials began to dominate the global economy. The CIA
helped overthrow any government which seemed to threaten US dominance.
In doing so, the brutal dictators and religious zealots to power: Mobutu
in the Congo, Saddam Hussein in Iraq, and let's not forget about Iran!
Believing it could shape the world in its image and ignoring all
complexities, the US invaded Iraq and gave power again to the tribal
leaders. Whoopsie daisy, they helped form ISIS. Facing the increasing
complexity of the world economy and against all evidence both Britain
and the US continue to indulge in the fantasy of a simpler natural
order. Britain votes to leave the EU and the US elects Donald Trump.

**Chinese Fears and Financial Control**

Jiang Qing commits suicide in prison, but casts a communist curse on
China. They will face unending evils! Deng Xioping died in 1997, casting
doubt on the country's future. What do they believe in besides money?
Can nationalistic purpose withstand the growing centrifugal forces of
individualism? China fears that the West is plotting to prevent their
rise to power. Real fears of the past are magnified and weaponised.
Instead of opium, now they fear being controlled by foreign capital.
Better go buy all of America's debt. This increases the value of the
dollar, lowers interest rates, and allows more borrowing to buy Chinese
goods. The US can't destroy China without also destroying itself.
Genius! Also, China demands Hong Kong from the British. The British
claim it will be bad for democracy. But Hong Kong was never a democracy
in the first place!

[*Part 6 --- Are We Pigeon? Or Are We
Dancer?*](https://thoughtmaybe.com/cant-get-you-out-of-my-head/#top)

**Thug Life Jihadis**

Tupak was raised by a former Black Panther radical. He wanted to fulfill
the vision of Black Power. But with the rise of individualism and
failures of previous movements, black idealism and visions of a better
world faded out of view. In their place came a "fairy land" which
distracted people away from their material problems and solutions with
cheap goods and entertainment. This individualism and the post-Black
Panther gangster attitude could never build the kind of solidarity
needed to fight the sources of power. All attempts to use art to bring
awareness to their problems only reinforced the growing cynical
attitudes.

At the same time, young men throughout the Middle East, Saudi Arabia in
particular, saw the rise of the same fairy land. The Saudi Royal Family
and other powerful oil interests exchanged religious tradition and a
life of faithful belief for money, extravagance, and cold calculation.
Where black radicals turned to cynical individualism, radical Islamists
turned to violent religious revolution. Though successful in their
organization and strategy, they were not immune from the rise of
individualism.

**The World is too Complicated for Revolution**

Psychologists began to suspect human consciousness was not a singular as
previously believed. There was growing evidence that humans had multiple
competing consciousnesses. After acting, we retroactively create stories
to justify what we did. If this is true, can humans really be trusted
with governing the world? Additionally, scientists were beginning to use
computers to model world systems like the climate, ecologies, and
economies. They found that small changes to variables could have
dramatic, unexpected consequences. These findings, on top of those found
about the human individual, increased the fears of human directed
political change. Instead of idealists trying to change the world, maybe
computer systems should stabilize it.

**Silicon Valley: Like Butte, But Instead of Copper, They're Mining
Human Behavior**

Silicon Valley struggled to find a way to monetize search and social
media platforms. Inspired by Ayn Rand individualism, SV entrepreneurs
believed they could use the market to bypass current systems of power.
It seems Google was the first platform to successfully monetize
personalized marketing through data collection. By using models, they
could determine which sorts of ads users would be likely to click on
based on past search data. This approach seemed to work and billions of
dollars started flowing in. As they began mining human behavior,
companies realized they could do more than just react to user
preferences, with psychological tactics, they could influence human
behavior. Though we've come to believe these companies can Control our
thoughts and behaviors, recent evidence suggests the techniques are not
so powerful. Though they can't easily influence behavior, it's clear
they can induce fear, suspicion, anxiety, and other "high arousal
emotions". Leading us to exhibit a lot of the cognitive distortions
defined by CBT. So we got to log off.

**Russian LARPing**

The Russian National Bolsheviks, led by former USSR dissident Eduard
Limonov, escalated their attempts to kickstart a nationalist revolution.
But foreshadowing leftist LARPers in the US, they had no real hold on
power or influence. I'm sure they felt good breaking into buildings and
going to jail though. Meanwhile, Putin rises to power as a blank slate.
Knowingly corrupt, but somehow able to combine rural discontent,
oligarch influence, and nationalist fear of the approaching west. The
Russians even have their own version of Hell's Angels. They no longer
believe in communism or democracy so they just seem really sad and
hopeless.

**China!**

While Silicon Valley builds power through private data collection and
the NSA tries to hide itself, China confidently consolidates its digital
power and looks to use it to control its population through "algorithmic
governance". Even those who appear to be tackling corruption end up
being just as guilty. Reformers struggle with the same dilemma as
elsewhere: should they embrace the cultural blank slate of greed and
finance capital or try to revive the nationalist story from the grave?

**Three Futures**

Curtis closes with 3 possible futures:

1.  China's brand of authoritarian collectivism, enforced by the soft
    power of computers and psychology and the hard power of a violent
    police state.
2.  A retreat to the past and an embrace of a nationalist state run by
    old elites (wot if the state guvna had to report to the local
    barons?).
3.  Or we can build a new future. Somehow this comes from building
    confidence (every dude gets unlimited Blue Chew?). Instead of
    allowing tech companies to use our anxiety as their raw material, we
    log off and rebuild connections.

In the words of Thelonius from the 2001 film *Shrek*: "Three! Pick
number three, my lord!"
